from re import template
from django.views.generic.list import ListView
from django.shortcuts import render
from django.views.generic.edit import CreateView

# Create your views here.
from meal_plans.models import MealPlan


class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(CreateView):
    model = MealPlan
    template_name = "meal_plans/create.html"
    fields = ["name", "owner", "recipes"]
