from django.contrib import admin
from meal_plans.models import MealPlan

# Register your models here.
# 1 import model
# 2 stub out the admin class
# 3 register both


class MealPlanAdmin(admin.ModelAdmin):
    pass


admin.site.register(MealPlan, MealPlanAdmin)
