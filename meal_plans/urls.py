from django.urls import path
from meal_plans.views import MealPlanListView, MealPlanCreateView

urlpatterns = [
    path("", MealPlanListView.as_view(), name="list_meal_plans"),
    path(
        "meal_plans/create/",
        MealPlanCreateView.as_view(),
        name="create_meal_plans",
    ),
]
